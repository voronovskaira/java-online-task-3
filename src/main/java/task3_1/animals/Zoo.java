package task3_1.animals;

import task3_1.animals.Animal;
import task3_1.animals.mammals.*;
import task3_1.animals.water_animals.DevilFish;
import task3_1.animals.water_animals.Dolphin;
import task3_1.animals.water_animals.Shark;
import task3_1.animals.water_animals.Turtle;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zoo {
    private List<Animal> animals = new ArrayList<Animal>();

    {
        animals.add(new Elephant("Lola", 10, "Gray", false, "Republic of Zimbabwe", false, "leaf"));
        animals.add(new Giraffe("Onix", 13, "Brown", true, "Republic of Kenya", false, "leaf"));
        animals.add(new Lion("Rudick", 8, "Brown", true, "Republic of Uganda", true, "meat"));
        animals.add(new Wolf("Zina", 9, "Gray", true, "Alaska", true, "meat"));
        animals.add(new Сougar("Camila", 11, "Black", true, "Nepal", true, "meat"));
        animals.add(new DevilFish("Viki", 2, "Black", false, "Australia", true, "shellfish and plankton"));
        animals.add(new Dolphin("Miki", 6, "Gray", false, "Australia", false, "fish and squid"));
        animals.add(new Shark("James", 7, "White", false, "New Zealand", true, "meat"));
        animals.add(new Turtle("Faster", 11, "Brown", false, "Australia", false, "fish"));
    }

    void sortAnimalsByColor(String color) {
        int animalCount = 0;
        for (Animal animal : animals) {
            if (animal.getColor().toLowerCase().equals(color.toLowerCase())) {
                System.out.println("We have " + animal + " in the zoo");
                animalCount++;
            }
        }
        if (animalCount <= 0)
            System.out.println("No search animals of this color");
    }

    void seeWhoHunter() {
        System.out.println("Hunters are: ");
        for (Animal animal : animals) {
            if (animal.isHunt())
                System.out.println(animal);
        }
    }

    void seeWhoNotHunter() {
        System.out.println("Not hunters are: ");
        for (Animal animal : animals) {
            if (!animal.isHunt())
                System.out.println(animal);
        }
    }

    void countAllAnimals() {
        System.out.println(animals.size() + " animals in the zoo");
    }

    void getAllAnimals(){
        System.out.println("Full information about animals in the zoo");
        for(Animal animal: animals){
            System.out.println(animal);
        }
    }
    void showWhatWhoEat(){
        System.out.println("Input 1 animal (Elephant/Giraffe/Lion/Wolf/Сougar/DevilFish/Dolphin/Shark/Turtle)");
        Scanner scan = new Scanner(System.in);
        String animalName = scan.nextLine();
        int animalCount = 0;
        for (Animal animal : animals) {
            if (animal.getClass().getSimpleName().equalsIgnoreCase(animalName)){
                System.out.println(animal.eat());
                animalCount++;
            }
        }
        if (animalCount <= 0)
            System.out.println("No search such animal");
    }

}
