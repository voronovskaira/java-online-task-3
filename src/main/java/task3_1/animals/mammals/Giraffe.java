package task3_1.animals.mammals;

public class Giraffe extends Mammal {
    public Giraffe(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        super(name, age, color, haveFur, countryOfOrigin, hunt, favoriteFood);
    }

    public String eat() {
        return "Giraffe like to eat " + favoriteFood;

    }

}
