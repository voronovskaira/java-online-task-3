package task3_1.animals.mammals;

public class Elephant extends Mammal{

    public Elephant(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        super(name, age, color, haveFur, countryOfOrigin, hunt, favoriteFood);
    }

    public String eat(){
        return "Elephant like to eat " + favoriteFood;
    }

}
