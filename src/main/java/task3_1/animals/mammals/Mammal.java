package task3_1.animals.mammals;

import task3_1.animals.Animal;

public abstract class Mammal extends Animal {
    public Mammal(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        super(name, age, color, haveFur, countryOfOrigin, hunt, favoriteFood);
    }
}


