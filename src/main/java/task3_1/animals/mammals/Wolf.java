package task3_1.animals.mammals;

public class Wolf extends Mammal {
    public Wolf(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        super(name, age, color, haveFur, countryOfOrigin, hunt, favoriteFood);
    }

    public String eat() {
        return "Wolf like to eat " + favoriteFood;

    }


}
