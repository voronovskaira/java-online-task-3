package task3_1.animals.mammals;

public class Lion extends Mammal {
    public Lion(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        super(name, age, color, haveFur, countryOfOrigin, hunt, favoriteFood);
    }

    public String eat() {
        return "Lion like to eat " + favoriteFood;
    }

}
