package task3_1.animals;

public abstract class Animal {
    protected String name;
    protected int age;
    protected String color;
    protected boolean haveFur;
    protected String countryOfOrigin;
    protected boolean hunt;
    protected String favoriteFood;

    public Animal(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        this.name = name;
        this.age = age;
        this.color = color;
        this.haveFur = haveFur;
        this.countryOfOrigin = countryOfOrigin;
        this.hunt = hunt;
        this.favoriteFood = favoriteFood;
    }

    public String getColor() {
        return color;
    }

    public boolean isHunt() {
        return hunt;
    }

    public String getFavoriteFood() {
        return favoriteFood;
    }

    public abstract String eat();


    @Override
    public String toString() {
        return this.getClass().getSimpleName() +"{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", color='" + color + '\'' +
                ", countryOfOrigin='" + countryOfOrigin + '\'' +
                '}';
    }


}
