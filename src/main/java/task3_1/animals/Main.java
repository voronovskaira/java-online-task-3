package task3_1.animals;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Zoo zoo = new Zoo();
        choose(zoo);
    }

    private static void choose(Zoo zoo) {
        System.out.println("Welcome!");
        while (true) {
            System.out.println(" Choose what you wanna do in the zoo\n" +
                    " 1 - if you want to see animals a certain color\n" +
                    " 2 - if you want to see who are hunter in the zoo\n" +
                    " 3 - if you want to see who not hunters in the zoo\n" +
                    " 4 - if you want to see how much animals in the zoo\n" +
                    " 5 - if you want to see full information about all animals\n" +
                    " 6 - if you want to see what who like to eat\n" +
                    " 0 - if you want to leave the zoo");
            Scanner scan = new Scanner(System.in);
            int value = scan.nextInt();

            switch (value) {
                case 1:
                    System.out.println("Input what color of animals you wanna to see in zoo (Hint: Gray/Brown/Black/White)");
                    scan.nextLine();
                    String color = scan.nextLine();
                    zoo.sortAnimalsByColor(color);
                    break;
                case 2:
                    zoo.seeWhoHunter();
                    break;
                case 3:
                    zoo.seeWhoNotHunter();
                    break;
                case 4:
                    zoo.countAllAnimals();
                    break;
                case 5:
                    zoo.getAllAnimals();
                    break;
                case 6:
                    zoo.showWhatWhoEat();
                    break;
                case 0:
                    System.out.println("Goodbye!");
                    System.exit(0);
                default:
                    System.out.println("You input wrong value, please choose from 1 to 4");
            }
        }
    }
}
