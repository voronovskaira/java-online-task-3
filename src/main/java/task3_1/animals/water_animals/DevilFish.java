package task3_1.animals.water_animals;

public class DevilFish extends WaterAnimal{
    public DevilFish(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        super(name, age, color, haveFur, countryOfOrigin, hunt, favoriteFood);
    }

    public String eat() {
        return "DevilFish like to eat " + favoriteFood;

    }

}
