package task3_1.animals.water_animals;

import task3_1.animals.Animal;

public abstract class WaterAnimal extends Animal {
    public WaterAnimal(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        super(name, age, color, haveFur, countryOfOrigin, hunt, favoriteFood);
    }
}
