package task3_1.animals.water_animals;

public class Dolphin extends WaterAnimal {
    public Dolphin(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        super(name, age, color, haveFur, countryOfOrigin, hunt, favoriteFood);
    }

    public String eat() {

        return "Dolphin like to eat " + favoriteFood;

    }

}
