package task3_1.animals.water_animals;

public class Turtle extends WaterAnimal {
    public Turtle(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        super(name, age, color, haveFur, countryOfOrigin, hunt, favoriteFood);
    }

    public String eat() {
        return "Turtle like to eat " + favoriteFood;
    }

}
