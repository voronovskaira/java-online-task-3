package task3_1.animals.water_animals;

public class Shark extends WaterAnimal {
    public Shark(String name, int age, String color, boolean haveFur, String countryOfOrigin, boolean hunt, String favoriteFood) {
        super(name, age, color, haveFur, countryOfOrigin, hunt, favoriteFood);
    }

    public String eat() {
        return "Shark like to eat " + favoriteFood;

    }

}
