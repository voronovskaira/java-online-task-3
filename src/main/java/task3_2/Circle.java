package task3_2;

public class Circle extends Shape {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }



    protected double countArea() {
        return Math.PI*Math.pow(radius, 2);
    }

    protected double countPerimeter() {
        return 2*Math.PI*radius;

    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                ", area=" + countArea() +
                ", perimeter=" + countPerimeter() +
                '}';
    }
}
