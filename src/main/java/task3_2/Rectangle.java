package task3_2;

public class Rectangle extends Shape {
    private double length;
    private double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    protected double countArea() {
        return length*width;
    }

    protected double countPerimeter() {
        return (length+width)*2;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "length= " + length +

                ", width=" + width +
                ", area= " + countArea() +
                ", perimeter= " + countPerimeter() +
                '}';
    }
}
