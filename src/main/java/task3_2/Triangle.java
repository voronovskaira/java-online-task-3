package task3_2;

public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    protected double countArea() {
        double s = countPerimeter() / 2;
        return Math.sqrt(s  *(s - a) * (s - b) * (s - c));
    }

    protected double countPerimeter() {
        return a+b+c;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", area= " + countArea() +
                ", perimeter= " + countPerimeter() +
                '}';
    }
}
