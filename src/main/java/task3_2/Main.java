package task3_2;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(5);
        System.out.println(circle);
        Rectangle rectangle = new Rectangle(6,12);
        System.out.println(rectangle);
        Triangle triangle = new Triangle(4,5,2);
        System.out.println(triangle);
    }
}
